#include<stdio.h>
#include<stdlib.h>

void accept(int*,int);
void selection(int*,int);
void display(int*,int);

void main()
{
	int arr[10],size;
	printf("\nEnter size upto 10 :");
	scanf("%d",&size);
	accept(arr,size);
	selection(arr,size);
	display(arr,size);
}

void accept(int*arr,int size)
{
	int i;
	printf("\nEnter values in array:");
	for(i=0;i<size;i++)
	scanf("%d",&arr[i]);
}

void selection(int*arr,int size)
{
	int i, j,low, t=0;
	for(i=0;i<size;i++)
	{
		low=i;
		for(j=i+1 ; j<size ; j++)
		{ 
			if(arr[j]<arr[low])
				low = j;
		}
		if(low !=i)
		{
			t = arr[i];
			arr[i]=arr[low];
			arr[low]=t;
		}
	}
}

void display(int *arr,int size) 
{
	int i;
	printf("\nSorted bro ,");
	for(i=0 ; i<size ; i++)
		printf("%d ",arr[i]);
	
}