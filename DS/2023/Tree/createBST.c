#include<stdio.h>
#include<stdlib.h>

typedef struct tree
{
	int no;
	struct tree* left;
	struct tree * right;
}t;

t* create(t*);
void preorder(t*);
void inorder(t*);
void postorder(t*);

int main()
{
	t * head = NULL;
	int ch=0;
	while(1)
	{   
		printf("\n***************************");
		printf("\n\tBinary Search Tree");
		printf("\n***************************");
		printf("\n\t 1.Create BST");
		printf("\n\t 2.Preorder");
		printf("\n\t 3.Inorder");
		printf("\n\t 4.Postorder");
		printf("\n\t 5.Exit");
		printf("\n***************************");
		printf("\nEnter the Choice : ");
		scanf("%d",&ch);
		
		switch(ch)
		{
			case 1: 
			head = create(head);
			break;
			case 2:
			preorder(head);
			break;
			case 3:
			inorder(head);
			break;
			case 4:
			postorder(head);
			break;
			case 5:
			exit(0);
			break;
			default:
			printf("\n Invalid choice...Enter valid Choice!");
			
		}
	}
	
	
	
	
	return 0;
}

t* create(t * head)
{
	t * nw=NULL, *p = head;
	int ans=0;
	do
	{
		nw = (t*)malloc(sizeof(t));
	printf("\n Enter the number : ");
	scanf("%d", &nw->no);
	nw->left = NULL;
	nw->right = NULL;
	
	if(head == NULL)
	{
		head = nw;
		p = head;
		printf("\n %d",p->right);
	 
	printf("\nin if 1");
	}
	
	else
	{
		while(p!=NULL)
		{			
			if(nw->no < p->no)
		  {
			  if(p->left != NULL)
			  {
				   p=p->left;
					printf("\nleft");
			  }
				 
			  else
				  break;
		  }
			  else
			  {
				if(nw->no > p->no)
				{
					if(p->right != NULL)
					{
						p=p->right;
					printf("\n right");
					}
						
					else
						break;
				}
				else
				{
					printf("\n\t Number is already exist");
					break;
				}
			  }
				  	  
		}
		if(nw->no < p->no)
		{
			p->left = nw;
			printf("\nplaced left");
		}
			
		else if(nw->no > p->no)
		{
			p->right = nw;
			printf("\nplaced right");
		}
			
		  
	}
		p=head;
		printf("\nDo you want more records? : ");
		scanf("%d",&ans);
	}while(ans == 1);
	return head;
}

void preorder(t * head)
{
	t * p = head;
	if(p!= NULL)
	{
		printf("\n");
		printf("\t%d", p->no);
		printf("\n");
		preorder(p->left);
		preorder(p->right);
	}
}

void inorder(t * head)
{
	t * p = head;
	if(p!= NULL)
	{
		inorder(p->left);
		printf("\n");
		printf("\t%d", p->no);
		printf("\n");
		inorder(p->right);
	}
}

void postorder(t * head)
{
	t * p = head;
	if(p!= NULL)
	{
		postorder(p->left);
		postorder(p->right);
		printf("\n");
		printf("\t%d", p->no);
		printf("\n");
	}
}