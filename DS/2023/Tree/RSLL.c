#include<stdio.h>
#include<stdlib.h>

typedef struct RSLL
{
	int no;
	char name[10];
	struct RSLL * next;
}sl;

sl* create(int);
void disp(sl*);

void main()
{
	sl* head;
	
	head = create(1);
	disp(head);
}

sl* create(int ans)
{
	sl* nw;
	
	if(ans==1)
	{
		nw = (sl*)malloc(sizeof(sl));
		printf("\n\tEnter the no and name :");
		scanf("%d%s",&nw->no,nw->name);
		printf("\n\t Do you want more records?(1/0) ");
		scanf("%d",&ans);
		nw->next = create(ans);
	}
	else
	return NULL;
	
	return nw;
}

void disp(sl* p)
{
	if(p!=NULL)
	{
	printf("\n\t%d %s",p->no,p->name);
	disp(p->next);
	}
	else
	exit(0);
}