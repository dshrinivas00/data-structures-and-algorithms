#include<stdio.h>
#include<stdlib.h>

typedef struct tree
{
	int no;
	struct tree* left;
	struct tree * right;
}t;

t* create(t*);
void count(t*,int *);
void countEO(t*, int* , int*);
void countLeafNode(t*,int *);
int search(t*, int,int);



int main()
{
	t * head = NULL;
	int ch=0, cnt=0, cntEven=0, cntOdd=0, cntLeafNode=0,key=0,flag = 0;
			while(1)
	{   
		printf("\n***************************");
		printf("\nBinary Search Tree Operations");
		printf("\n***************************");
		printf("\n\t 1.Create BST");
		printf("\n\t 2.Count of Nodes");
		printf("\n\t 3.Count of Even and Odd nodes");
		printf("\n\t 4.Count of Leaf Nodes");
		printf("\n\t 5.Search element (with recurrsion)");
		printf("\n\t 6.Search element (without recurrsion)");
		printf("\n\t 7.Exit");
		printf("\n***************************");
		printf("\nEnter the Choice : ");
		scanf("%d",&ch);
		
		switch(ch)
		{
			case 1: 
			head = create(head);
			break;
			case 2:
			count(head, &cnt);
			printf("\n\t the count of nodes is %d",cnt);
			break;
			case 3:
			countEO(head,&cntEven,& cntOdd);
			printf("\n\t the count of Even nodes is %d \n\tthe count of odd nodes is %d",cntEven,cntOdd);
			break;
			case 4:
			countLeafNode(head,&cntLeafNode);
			printf("\n\t the count of Leaf nodes is %d",cntLeafNode);
			break;
			case 5:
			printf("\nEnter the key to search :");
			scanf("%d",&key);
			flag = search(head,key,flag);
			if(flag = 1)
				printf("\n\t Key Found");
			else
				printf("\n\t key not found");
			break;
			case 6:
			break;
			case 7:
			exit(0);
			break;
			default:
			printf("\n Invalid choice...Enter valid Choice!");
			
		}
	}
			head = create(head);		
			count(head, &cnt);	
			printf("\n\t the count of nodes is %d",cnt);
	
	return 0;
}

t* create(t * head)
{
	t * nw=NULL, *p = head;
	int ans=0;
	do
	{
		nw = (t*)malloc(sizeof(t));
	printf("\n Enter the number : ");
	scanf("%d", &nw->no);
	nw->left = NULL;
	nw->right = NULL;
	
	if(head == NULL)
	{
		head = nw;
		p = head;
		printf("\n %d",p->right);
	 
	printf("\nin if 1");
	}
	
	else
	{
		while(p!=NULL)
		{			
			if(nw->no < p->no)
		  {
			  if(p->left != NULL)
			  {
				   p=p->left;
					printf("\nleft");
			  }
				 
			  else
				  break;
		  }
			  else
			  {
				if(nw->no > p->no)
				{
					if(p->right != NULL)
					{
						p=p->right;
					printf("\n right");
					}
						
					else
						break;
				}
			  }
				  	  
		}
		if(nw->no < p->no)
		{
			p->left = nw;
			printf("\nplaced left");
		}
			
		else if(nw->no > p->no)
		{
			p->right = nw;
			printf("\nplaced right");
		}
			
		  
	}
		p=head;
		printf("\nDo you want more records? : ");
		scanf("%d",&ans);
	}while(ans == 1);
	return head;
}

//count node by call by reference 

void count(t* head,int* cnt)
{
	t * p = head;
	if(p!= NULL)
	{
		count(p->left,cnt);
		printf("\n");
		printf("\t%d", p->no);
		*cnt = *cnt + 1;
		printf("\n");
		count(p->right,cnt);
	}
}

void countEO(t * head, int * cntEven, int * cntOdd)
{
	t * p = head;
	if(p!= NULL)
	{
		countEO(p->left,cntEven,cntOdd);
		if(p->no%2==0)
			*cntEven = *cntEven+1;
		else if(p->no % 2 !=0)
			*cntOdd = *cntOdd+1;
		countEO(p->right,cntEven,cntOdd);
	}
}

void countLeafNode(t * head,int * cntLeafNode)
{
	t * p = head;
	if(p!= NULL)
	{
		countLeafNode(p->left,cntLeafNode);
		if(p->left == NULL && p->right==NULL)
			*cntLeafNode = *cntLeafNode+1;
		countLeafNode(p->right,cntLeafNode);
	}
}

int search(t * head,int key,int flag)
{
	t * p = head;
	if(p!= NULL)
	{
		search(p->left,key,flag);
		if(p->no == key)
			flag = 1;
		search(p->right,key,flag);
	}
	return flag;
}