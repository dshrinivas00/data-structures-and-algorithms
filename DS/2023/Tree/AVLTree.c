#include<stdio.h>
#include<stdlib.h>

typedef struct AVLTree
{
	int no, height;
	struct AVLTree * left;
	struct AVLTree * right;
}AVL;

AVL * createAVL(AVL*);
void disp(AVL*);
AVL * calHeight(AVL*);
AVL * balanceFactor(AVL * , AVL*);
AVL * checkRotation(AVL* , AVL* );
AVL* LL(AVL* , AVL* ,AVL *);
AVL * RR(AVL* , AVL* ,AVL * );
AVL * LR(AVL* , AVL* ,AVL * );
AVL* RL(AVL* , AVL* ,AVL * );


AVL* calHeight(AVL*root)
{
	AVL * p = root;
	int htleft = p->left->height, htright = p->right->height;
	while(p!= NULL)
	{
		calHeight(p->left);
		calHeight(p->right);
		if(htleft > htright)
		{
			p->height = htleft + 1;
			printf("\n %d node's height is %d",p->no, p->height);
		}
					
		else
		{
			p->height = htleft + 1;
			printf("\n %d node's height is %d",p->no, p->height);
		}
		
		root = balanceFactor(p,root);
		
	}
	return root;
}

AVL * balanceFactor(AVL * p, AVL* root)
{
	int bf = (p->left->height) - (p->right->height);
	
	if(bf < -1 || bf > 1)
		root = checkRotation(p,root);
	
	return root;
}

AVL * checkRotation(AVL* p, AVL* root)
{
	AVL*temp1 = p, *temp2=root;
	int check, i;
	for(i=0 ; i<2 ; i++)
	{
		if(temp1->left->height > temp1->right->height)
		check=(i * 10)+1;
		else if(temp1->left->height < temp1->right->height)
			check = (i * 10)+2;
	}
	
	switch(check)
	{
		case 11:
		root = LL(p,root,temp2);
		break;
		case 22:
		root = RR(p,root,temp2);
		break;
		case 12:
		root = LR(p,root,temp2);
		break;
		case 21:
		root = RL(p, root,temp2);
		break;
		default:
		printf("\n Invalid");
	}
	return root;
}
	
AVL* LL(AVL* p, AVL* root,AVL * temp2)
	{
		
		 if(root == p)
		 {
			 p = root->left;
			 p->right = root;
			 root->left = NULL;
			 root = p;
			 return root;
		 }
		 else
		 {	
			if(temp2->left==p)
			 {
				 temp2->left = p->left;
				 temp2->left-> right = p;
				 p->left = NULL;
				 return root;
			 }
			 LL(p,root,temp2->left);
			 LL(p,root,temp2->right);
			 
		 }
	}
	
	AVL * RR(AVL* p, AVL* root,AVL * temp2)
	{
		 if(root == p)
		 {
			 p = root -> right;
			 p -> left = root;
			 root -> right = NULL;
			 root = p;
			 return root;
		 }
		 else
		 {
			 if(temp2->right==p)
			 {
				 temp2->right = p->right;
				 temp2->right-> left = p;
				 p->right = NULL;
				 return root;
			 }
			 RR(p,root,temp2->left);
			 RR(p,root,temp2->right);
			 
		 }
	}
	
	AVL * LR(AVL* p, AVL* root,AVL * temp2)
	{
		 AVL* t;
		 int flag = 0;
		 if(root == p)
		 {
			 root = p->left->right;
			  t->left = root->left;
			  t->right = root-> right;
			  t->no = root->no;
			  root -> left = p -> left;
			  root -> right = p;
			  flag = 1;
		 }
		 else
		 {
			 if(temp2 -> left == p || temp2 -> right == p )
			 {
				 p = p->left->right;
				 t->left = p->left;
				 t->right = p->right;
				 t->no = p->no;
				 p->left = temp2->left->left;
				 p->right = temp2->left;
				 temp2->left = p;
				 p->left->right = NULL;
				 flag = 1;
				 
			 }
			 if(flag == 1)
			 {
				 for(p=root;p!=NULL;)
			 {
				if(t->no < p->no)
				{
					if(p->left == NULL)
					{
						p->left = t;
						break;
					}
					
						   
					p=p->left;
				}
				else
				{
					if(t->no > p->no)
					{ 
						if(p->right == NULL)
						{
							p->right = t;
							break;
						}
						p=p->right;
					}
				}
			 }
			 return root;
				 
			 }
			 
			 LR(p,root,temp2->left);
			 LR(p,root,temp2->right);
		 }
	}
	
	AVL* RL(AVL* p, AVL* root,AVL * temp2)
	{
		AVL * t;
		 int flag = 0;
		 if(root == p)
		 {
			 root = p->right->left;
			  t->left = root->left;
			  t->right = root-> right;
			  t->no = root->no;
			  root -> right = p -> right;
			  root -> left = p;
			  flag = 1;
		 }
		 else
		 {
			 if(temp2 -> left == p || temp2 -> right == p )
			 {
				 p = p->right->left;
				 t->left = p->left;
				 t->right = p->right;
				 t->no = p->no;
				 p->right = temp2->right->right;
				 p->left = temp2->right;
				 temp2->right = p;
				 p->right->left = NULL;
				 flag = 1;
				 
			 }
			 if(flag == 1)
			 {
				 for(p=root;p!=NULL;)
			 {
				if(t->no < p->no)
				{
					if(p->left == NULL)
					{
						p->left = t;
						break;
					}
					
						   
					p=p->left;
				}
				else
				{
					if(t->no > p->no)
					{ 
						if(p->right == NULL)
						{
							p->right = t;
							break;
						}
						p=p->right;
					}
				}
			 }
			 return root;
				 
			 }
			 
			 LR(p,root,temp2->left);
			 LR(p,root,temp2->right);
		 }
	}
	

AVL * createAVL(AVL * root)
{
	AVL * p = root , * nw =NULL;
	int ch;
	
	do{
		nw = (AVL*)malloc(sizeof(AVL));
		nw->left=NULL;
		nw->right=NULL;
		printf("\n\t Enter the no : ");
		scanf("%d",&nw->no);
		nw->height = 1;
		
		if(root == NULL)
			root = nw;
		
		else{
			 for(p=root;p!=NULL;)
			 {
				if(nw->no < p->no)
				{
					if(p->left == NULL)
					{
						p->left = nw;
						break;
					}
					
						   
					p=p->left;
				}
				else
				{
					if(nw->no > p->no)
					{ 
						if(p->right == NULL)
						{
							p->right = nw;
							break;
						}
						p=p->right;
					}
				}
			 }
			 
			 calHeight(root);
		}
		printf("\n\t Do you want more records?(1/0) : ");
		scanf("%d",&ch);
	}while(ch == 1);
	
	return root;
}

void disp(AVL * root)
{
	AVL* p = root;
	if(p != NULL)
	{
		printf("%d", p->no);
		disp(p->left);
		disp(p->right);
	}
}


int main()
{
	AVL * root=NULL;
	root = createAVL(root);
	printf("\n\t AVL tree created");
	disp(root);
}

