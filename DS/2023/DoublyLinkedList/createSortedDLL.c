#include<stdio.h>
#include<conio.h>
#include<malloc.h>
#include <string.h>

typedef struct doubleLinkedList
{
	int no;
	char name[20];
	struct doubleLinkedList * nxt;
	struct doubleLinkedList * prev;
}s;

s* create1(s*);
void disp(s*); 



int main()
{
	s*start=NULL;
	start = create1(start);
	disp(start);
	
	
	return 0;
}

s* create1(s*p)
{
	s* nw=NULL,*lst=p;
	int ans=0;
	
	do
	{
		nw = (s*)malloc(sizeof(s));
		nw->nxt = NULL;
		printf("Enter int value : ");
		scanf("%d",&nw->no);
		printf("Enter name : ");
		scanf("%s",nw->name);
		nw->prev = NULL;
		
		if(lst == NULL)
		{
			lst = nw;
			p = lst;
		}
		
		else 
		{
			if(nw->no > lst->no)
			{
				for(;p->nxt!= NULL && p->nxt->no < nw->no;p=p->nxt);
				
				if(p->nxt != NULL)
				{
					nw->nxt = p->nxt;
					nw->prev = p;
					p->nxt= nw;
					nw->nxt->prev=nw;					
				}
				else
				{
					p->nxt = nw;
					nw->prev = p;					
				}
				p=lst;
			}
			else if(nw->no < lst->no)
			{
				for(;p->prev!= NULL && p->prev->no > nw->no;p=p->prev);
				
				if(p->prev != NULL)
				{
					nw->nxt = p;
					nw->prev = p->prev;
					p->prev= nw;
					nw->prev->nxt=nw;					
				}
				else
				{
					p->prev = nw;
					nw->nxt = p;					
				}
				p=lst;
			}
		}
		
		
		printf("Do you want more nodes? (1/0): ");
		scanf("%d",&ans);
		
	}while(ans==1);
	for(;p->prev!=NULL;p=p->prev);
	return p;
}



void disp(s*p)
{
	printf("\n\t ****Sorted DLL in ascending order****");
	for(;p!=NULL ; p=p->nxt)
	{
		printf("\n\t%d %s",p->no,p->name);
	}
}
