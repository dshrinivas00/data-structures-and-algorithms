#include<stdio.h>
#include<conio.h>
#include<malloc.h>
#include <string.h>

typedef struct doubleLinkedList
{
	int no;
	char name[20];
	struct doubleLinkedList * nxt;
	struct doubleLinkedList * prev;
}s;

s* create1(s*head);
void disp(s*p); 
s* delete(s*, int);


int main()
{
	s*start=NULL, * head= NULL;
	int key;
	start = create1(start);
	disp(start);
	printf("\n\t Enter key to delete : ");
	scanf("%d",&key);
	head = delete(start,key);
	disp(head);

	
	
	return 0;
}

s* create1(s*head)
{
	s* nw=NULL,*lst=NULL;
	int ans=0;
	
	do
	{
		nw = (s*)malloc(sizeof(s));
		nw->nxt = NULL;
		printf("Enter int value : ");
		scanf("%d",&nw->no);
		printf("Enter name : ");
		scanf("%s",nw->name);
		nw->prev = NULL;
		
		if(head == NULL)
		head = nw;
		
		else 
		{
			lst->nxt = nw;
			nw->prev = lst;
		}
		lst = nw;
		
		printf("Do you want more nodes? (1/0): ");
		scanf("%d",&ans);
		
	}while(ans==1);
	return head;
}

s* delete(s* head,int key)
{
	s* p = head;
	
		if(head->no == key)
		{
			head = head->nxt;
			head->prev = NULL;
			free(p);
			printf("\n\t key node deleted(head)");
			return head;
		}
		else
		{
			for(;p!=NULL && p->no!=key ;p=p->nxt);
			
			if(p!=NULL)
			{
					if(p->nxt == NULL)
			{
				p->prev->nxt=NULL;
				free(p);
				printf("\n\t key node deleted(end)");
				return head;
			}
			p->nxt->prev = p->prev;
			p->prev->nxt = p->nxt;
			free(p);
			
				printf("\n\t key node deleted (middle)");
				return head;
			}
			else
			{
				printf("\n\tInvalid key");
				return head;
			}		
		}	
	
}

void disp(s*p)
{
	for(;p!=NULL ; p=p->nxt)
	{
		printf("\n%d %s",p->no,p->name);
	}
}
