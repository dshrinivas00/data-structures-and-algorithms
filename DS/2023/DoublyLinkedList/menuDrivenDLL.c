#include<stdio.h>
#include<conio.h>
#include<malloc.h>
#include <string.h>

typedef struct doubleLinkedList
{
	int no;
	char name[20];
	struct doubleLinkedList * nxt;
	struct doubleLinkedList * prev;
}s;

s* create(s*);
s* insertion(s*,int);
s* deletion(s*,int);
void rev(s*);
void disp(s*); 

int main()
{
	s*start=NULL,*head=NULL;
	int ch,pos;
	
	while(1)
	{
		printf("\n******* DLL Menu Driven *******");
		printf("\n\t 1.create");
		printf("\n\t 2.Insert");
		printf("\n\t 3.Delete");
		printf("\n\t 4.Reverse");
		printf("\n\t 5.Print");
		printf("\n\t 6.Exit");
		printf("\n********************************\n");
	
		printf("\n\t Enter your choice : ");
		scanf("%d",&ch);
	
		switch(ch)
		{
			case 1://Create
				start =create(start);
				break;
			case 2://Insert
				printf("\n\t Enter the position : ");
				scanf("%d",&pos);
				start = insertion(start,pos);
				break;
			case 3://Delete
				printf("\n\t Enter the position : ");
				scanf("%d",&pos);
				start = deletion(start,pos);
				break;
			case 4://Reverse
				rev(start);
				break;
			case 5://Print
				disp(start); 
				break;
			case 6:
				exit(0);
			default:
				printf("\n\t Invalid Choice");

		}
	}
	return 0;
}

s* create(s*head)
{
	s* nw=NULL,*lst=NULL;
	int ans=0;
	
	do
	{
		nw = (s*)malloc(sizeof(s));
		nw->nxt = NULL;
		printf("Enter int value : ");
		scanf("%d",&nw->no);
		printf("Enter name : ");
		scanf("%s",nw->name);
		nw->prev = NULL;
		
		if(head == NULL)
		head = nw;
		
		else 
		{
			lst->nxt = nw;
			nw->prev = lst;
		}
		lst = nw;
		
		printf("Do you want more nodes? (1/0): ");
		scanf("%d",&ans);
		
	}while(ans==1);
	return head;
}
s* insertion(s* head,int pos)
{
	s* p = head,*nw=NULL;
	int i;
	if(p!= NULL)
	{
		if(pos==1)
		{
			nw = (s*)malloc(sizeof(s));
			printf("\n\tEnter No and Name : ");
			scanf("%d%s",&nw->no,nw->name);
			nw->nxt = p;
			p->prev = nw;
			nw->prev = NULL;
			head = nw;
			printf("\n\t Inserted at head");
			return head;
		}
		else
		{
			for(i =1;p!=NULL && i<pos-1 ;p=p->nxt,i++);
			
			if(p!=NULL)
			{
				nw = (s*)malloc(sizeof(s));
				printf("Enter No and Name : ");
				scanf("%d%s",&nw->no,nw->name); 
				nw->nxt = p->nxt;
				nw->prev=p;
				p->nxt=nw;
				if(nw->nxt == NULL)
			{
				printf("\n\t Inserted at end");
				return head;
			}
				printf("\n\t Inserted at middle");
				return head;
			}
			else
			{
				printf("\n\tInvalid insertion");
				return head;
			}	
		}
	}
	else{
		printf("\n\tEmpty Linked List");
		return head;
	}
}

s* deletion(s* head,int pos)
{
	s* p = head;
	int i;
	if(p!= NULL)
	{
		if(pos==1)
		{
			head = head->nxt;
			head->prev = NULL;
			free(p);
			printf("\n\t deleted head node");
			return head;
		}
		else
		{
			for(i =1;p!=NULL && i<pos ;p=p->nxt,i++);
			
			if(p!=NULL)
			{
					if(p->nxt == NULL)
			{
				p->prev->nxt=NULL;
				free(p);
				printf("\n\t deleted at end");
				return head;
			}
			p->nxt->prev = p->prev;
			p->prev->nxt = p->nxt;
			free(p);
			
				printf("\n\t deleted at middle");
				return head;
			}
			else
			{
				printf("\n\tInvalid position");
				return head;
			}		
		}	
	}
	else{
		printf("\n\tEmpty Linked List");
		return head;
	}
}
void rev(s*p)
{
	s* head = p;
	if(p!=NULL)
	{
		for(;p->nxt!= NULL; p=p->nxt);
	for(;p!=NULL; p=p->prev)
	{
		printf("\n%d %s",p->no,p->name);
	}
	}
	else{
		printf("\n\t Linked List is Empty...");	
	}
	
	
}

void disp(s*p)
{
	if(p==NULL)
	{
		printf("\n\t Linked List is Empty...");
	}
	else
	{
		for(;p!=NULL ; p=p->nxt)
		{
			printf("\n%d %s",p->no,p->name);
		}
	}
	
} 