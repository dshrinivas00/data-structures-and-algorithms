#include<stdio.h>
#include<conio.h>
#include<malloc.h>
#include <string.h>

typedef struct doubleLinkedList
{
	int no;
	char name[20];
	struct doubleLinkedList * nxt;
	struct doubleLinkedList * prev;
}s;

s* create1(s*head);
void disp(s*p); 
s* join(s* , s*);

int main()
{
	s*start=NULL, *head1=NULL, *head2=NULL;
	printf("\n\t Create 1st DLL : ");
	head1 = create1(head1);
	printf("\n\t Create 2nd DLL : ");
	head2 = create1(head2);
	start = join(head1, head2);
	disp(start);

	
	
	return 0;
}

s* create1(s*head)
{
	s* nw=NULL,*lst=NULL;
	int ans=0;
	
	do
	{
		nw = (s*)malloc(sizeof(s));
		nw->nxt = NULL;
		printf("Enter int value : ");
		scanf("%d",&nw->no);
		printf("Enter name : ");
		scanf("%s",nw->name);
		nw->prev = NULL;
		
		if(head == NULL)
		head = nw;
		
		else 
		{
			lst->nxt = nw;
			nw->prev = lst;
		}
		lst = nw;
		
		printf("Do you want more nodes? (1/0): ");
		scanf("%d",&ans);
		
	}while(ans==1);
	return head;
}

s* join(s*head1, s*head2)
{
	s*p = head1;
	for(;head1->nxt!=NULL ; head1= head1->nxt);
	
	head1->nxt =head2;
	head2->prev = head1;
	return p;
}

void disp(s*p)
{
	printf("\n\t ***joint DLL***");
	for(;p!=NULL ; p=p->nxt)
	{
		printf("\n\t %d %s",p->no,p->name);
	}
}
