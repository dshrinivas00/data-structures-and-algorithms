#include<stdio.h>
#include<conio.h>
#include<malloc.h>
#include <string.h>

typedef struct doubleLinkedList
{
	int no;
	char name[20];
	struct doubleLinkedList * nxt;
	struct doubleLinkedList * prev;
}s;

s* create1(s*head);
void disp(s*p); 
s* insert(s*, int);


int main()
{
	s*start=NULL, * head= NULL;
	int pos;
	start = create1(start);
	disp(start);
	printf("\n\t Enter position to insert : ");
	scanf("%d",&pos);
	head = insert(start,pos);
	disp(head);

	
	
	return 0;
}

s* create1(s*head)
{
	s* nw=NULL,*lst=NULL;
	int ans=0;
	
	do
	{
		nw = (s*)malloc(sizeof(s));
		nw->nxt = NULL;
		printf("Enter int value : ");
		scanf("%d",&nw->no);
		printf("Enter name : ");
		scanf("%s",nw->name);
		nw->prev = NULL;
		
		if(head == NULL)
		head = nw;
		
		else 
		{
			lst->nxt = nw;
			nw->prev = lst;
		}
		lst = nw;
		
		printf("Do you want more nodes? (1/0): ");
		scanf("%d",&ans);
		
	}while(ans==1);
	return head;
}

s* insert(s* head,int pos)
{
	s* p = head,*nw=NULL;
	int i;
	if(p!= NULL)
	{
		if(pos==1)
		{
			nw = (s*)malloc(sizeof(s));
			printf("\n\tEnter No and Name : ");
			scanf("%d%s",&nw->no,nw->name);
			nw->nxt = p;
			p->prev = nw;
			nw->prev = NULL;
			head = nw;
			printf("\n\t Inserted at head");
			return head;
		}
		else
		{
			for(i =1;p!=NULL && i<pos-1 ;p=p->nxt,i++);
			
			
				nw = (s*)malloc(sizeof(s));
				printf("Enter No and Name : ");
				scanf("%d%s",&nw->no,nw->name); 
				nw->nxt = p->nxt;
				nw->prev=p;
				p->nxt=nw;
				if(nw->nxt == NULL)
			{
				printf("\n\t Inserted at end");
				return head;
			}
				printf("\n\t Inserted at middle");
				return head;
			
		}
		printf("\n\tInvalid insertion");
		return head;
	}
	else{
		printf("\n\tEmpty Linked List");
		return head;
	}
}

void disp(s*p)
{
	for(;p!=NULL ; p=p->nxt)
	{
		printf("\n%d %s",p->no,p->name);
	}
}
