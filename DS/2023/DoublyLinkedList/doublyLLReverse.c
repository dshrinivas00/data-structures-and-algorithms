#include<stdio.h>
#include<conio.h>
#include<malloc.h>
#include <string.h>

typedef struct doubleLinkedList
{
	int no;
	char name[20];
	struct doubleLinkedList * nxt;
	struct doubleLinkedList * prev;
}s;

s* create1(s*head);
void disp(s*p); 
void rev(s*);

int main()
{
	s*start=NULL;
	start = create1(start);
	disp(start);
	printf("\n\t Printing in reverse order using prev pointer");
	rev(start);
	
	
	return 0;
}

s* create1(s*head)
{
	s* nw=NULL,*lst=NULL;
	int ans=0;
	
	do
	{
		nw = (s*)malloc(sizeof(s));
		nw->nxt = NULL;
		printf("Enter int value : ");
		scanf("%d",&nw->no);
		printf("Enter name : ");
		scanf("%s",nw->name);
		nw->prev = NULL;
		
		if(head == NULL)
		head = nw;
		
		else 
		{
			lst->nxt = nw;
			nw->prev = lst;
		}
		lst = nw;
		
		printf("Do you want more nodes? (1/0): ");
		scanf("%d",&ans);
		
	}while(ans==1);
	return head;
}



void disp(s*p)
{
	for(;p!=NULL ; p=p->nxt)
	{
		printf("\n%d %s",p->no,p->name);
	}
}

void rev(s*p)
{
	int i;
	for(;p->nxt!=NULL;p=p->nxt);
	for(;p!=NULL ; p=p->prev)
		printf("\n%d %s",p->no,p->name);
}