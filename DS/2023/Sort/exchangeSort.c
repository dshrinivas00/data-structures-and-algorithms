#include<stdio.h>
#include<stdlib.h>

int * exchangeSort(int*, int);
void disp(int *, int);

int main()
{
	int size, *arr=NULL, i;
	
	printf("\nEnter the size of an array : ");
	scanf("%d", &size);
	
	arr = (int*)malloc(size*sizeof(int));
	printf("\n Enter the elements in an array : ");
	
	for(i=0; i<size ; i++)
		scanf("%d",&arr[i]);
	printf("\n Given Array : ");
	for(i=0; i<size ;i++)
		printf("%d ",arr[i]);
	
	arr = exchangeSort(arr,size);
	disp(arr, size);
}

int * exchangeSort(int * arr, int size)
{
	int i, j, temp;
	for(i = 0 ; i < size ; i++)
	{
		for(j = i+1 ; j < size ; j++)
		{
			if(arr[i]>arr[j])
			{
				temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
		}
	}
	return arr;
}

void disp(int * arr, int size)
{
	int i;
	printf("\nSorted Array : ");
	for(i=0; i<size ;i++)
		printf("%d ",arr[i]);
		
}

