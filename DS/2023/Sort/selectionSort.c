#include<stdio.h>
#include<stdlib.h>

int * selectionSort(int*, int);
void disp(int *, int);

int main()
{
	int size, *arr=NULL, i;
	
	printf("\nEnter the size of an array : ");
	scanf("%d", &size);
	
	arr = (int*)malloc(size*sizeof(int));
	printf("\n Enter the elements in an array : ");
	
	for(i=0; i<size ; i++)
		scanf("%d",&arr[i]);
	printf("\n Given Array : ");
	for(i=0; i<size ;i++)
		printf("%d ",arr[i]);
	
	arr = selectionSort(arr,size);
	disp(arr, size);
}

int * selectionSort(int * arr, int size)
{
	int i, j, low = size, temp;
	for(i = 0 ; i < size ; i++)
	{
		low = i;
		for(j = i+1 ; j < size ; j++)
		{
			if(arr[low]>arr[j])
				low = j;
		}
		
		if(i == low)
			continue;
		else 
			{
				temp = arr[i];
				arr[i] = arr[low];
				arr[low] = temp;
			}
	}
	return arr;
}

void disp(int * arr, int size)
{
	int i;
	printf("\nSorted Array : ");
	for(i=0; i<size ;i++)
		printf("%d ",arr[i]);
		
}

