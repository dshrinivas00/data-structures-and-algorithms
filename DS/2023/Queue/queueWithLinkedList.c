#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<process.h>

typedef struct queue
{
	int no;
	char name[10];
	struct queue * next;
}que;

que* insert(que*);
que* delete(que*);
void disp(que*);

int main()
{
	que*rear=NULL, *front=NULL;
	int ch; 

    while(1)
    {
	printf("\n********************************");
	printf("\nStack With Linked List");
	printf("\n********************************");
	printf("\n 1.insert");
	printf("\n 2.delete");
	printf("\n 3.Display");
	printf("\n 4.Exit");
	printf("\n********************************");
	printf("\nEnter the choice : ");
	scanf("%d",&ch);


	switch(ch)
	{
		case 1 : rear = insert(rear);
				if(front==NULL)
					front=rear;
		         break;
		case 2 : front = delete(front);
		         break;
		case 3 : disp(front);
		         break;
		default : exit(0);


	}

    }
	return 0;
}

que* insert(que*rear)
{
	que* nw;

	nw = (que*)malloc(sizeof(que));
	printf("Enter no and name : ");
	scanf("\n%d%s",&nw->no,nw->name);
	if(rear!=NULL)
	rear->next=nw;
	rear=nw;

	return rear;
}

que* delete(que*front)
{
	que*p;
	if(front==NULL)
		printf("\nQueue is Empty");
	
	else
	{
		p=front->next;
		free(front);
		front=p;
   

	}
	return front;
}

void disp(que*front)
{
	for(;front !=NULL;front=front->next)
		printf("\n%d%s",front->no,front->name);
}