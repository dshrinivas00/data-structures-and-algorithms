#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<process.h>

typedef struct queue
{
	int no;
	char name[10];
	struct queue * next;
}que;

que* enque(que**,que*);
que* deque(que**,que*);
void disp(que*,que*);

int main()
{
	que*rear=NULL, *front=NULL;
	int ch; 

    while(1)
    {
	printf("\n********************************");
	printf("\Circular Queue With Linked List");
	printf("\n********************************");
	printf("\n 1.Enque");
	printf("\n 2.Deque");
	printf("\n 3.Display");
	printf("\n 4.Exit");
	printf("\n********************************");
	printf("\nEnter the choice : ");
	scanf("%d",&ch);


	switch(ch)
	{
		case 1 : rear = enque(&front,rear);
				if(front==NULL)
					front=rear;
		         break;
		case 2 : rear = deque(&front,rear);
		         break;
		case 3 : disp(rear,front);
		         break;
		default : exit(0);


	}

    }
	return 0;
}

que * enque(que**front, que * rear)
{
	que * nw;
	nw = (que*)malloc(sizeof(que));
	printf("\n\tEnter no and name : ");
	scanf("%d %s", &nw->no, nw->name);
	
	if(*front == NULL)
	{
		*front = nw;
		nw->next = nw;
		printf("\n\t%d is added in queue(1)",nw->no);
	}
	else
	{
		nw->next = rear ->next;
		rear->next =nw;	
		printf("\n\t%d is added in queue(2)",nw->no);
	}
	rear = nw;
	return rear;

}

que* deque(que**front,que*rear)
{
	que* p = *front;
	if(* front == NULL)
	{
		printf("\n\tCircular Queue is Empty");
		return rear;
	}
		
	else
	{
		if(*front == rear)
		{
			printf("\n\t%d is deleted in queue(1)",p->no);
			free(*front);
			*front=NULL;
			rear=NULL;
			return rear;
		}
		else
		{
			rear->next =p->next;
			*front = p->next;
			printf("\n\t%d is deleted in queue(2)",p->no);
			free(p);
			p=*front;
			return rear;
		}
	}
}

void disp(que*rear,que* front)
{
	que * p = rear;
	if(rear == NULL)
		printf("\n\tList is Empty");
	else{
		do{
			p=p->next;
			printf("\n\t %d %s",p->no,p->name);
			if(front == p)
			printf("\t front");
		if(rear ==  p)
			printf("\t rear");
		
		}while(p!=rear);
	}
	
}

