#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<process.h>
#define MAX 5

typedef struct queue
{
	int no;
	char name[10];
	struct queue * next;
}que;

int enque(que*,int,int);
int deque(que*,int*, int);
void disp(que*, int , int);

int main()
{
	que str[MAX];
	int rear = -1, front = 0;
	int ch; 

    while(1)
    {
	printf("\n********************************");
	printf("\n\tCircular queue with Array");
	printf("\n********************************");
	printf("\n 1.Enqueue");
	printf("\n 2.Dequeue");
	printf("\n 3.Display");
	printf("\n 4.Exit");
	printf("\n********************************");
	printf("\nEnter the choice : ");
	scanf("%d",&ch);


	switch(ch)
	{
		case 1 :  rear = enque(str ,rear,front);
		         break;
		case 2 : front = deque(str,&rear, front);
		         break;
		case 3 : disp(str, rear, front);
		         break;
		case 4 : exit(0);
		         break;
		default : 
				printf("\n Invalid Choice");

	}
}


	return 0;
}

int enque(que * str, int rear, int front)
{
	if((rear == (MAX-1)&& front == 0) || ((rear+1)== front && (rear!=-1)  ))
	{
		printf("\n\t Queue is full\n");
		return rear;
	}
		else
		{
			if(rear == (MAX-1))
				rear = 0;
			else
				rear=rear+1;
			printf("\n Enter the values : ");
			scanf("\n%d %s",&str[rear].no,str[rear].name);
			printf("\n Enqueue Successful\n");
			return rear;
		}
}

int  deque(que * str, int *rear, int front)
{
	if( front == 0 && *rear ==-1 )
	{
		printf("\n Queue is Empty\n");
		return front;
	}
	if(front == *rear)
	{
		front = 0;
		*rear = -1;
		printf("\n dequeue Successful\n");
		return front;
	}
	if(front == (MAX-1))
		front = 0;
	else
		front++;
	printf("\n dequeue Successful\n");
	return front;
}

void disp(que * str, int rear, int front)
{
	int i = front;
	while(1)
	{
		if(front == 0 && rear == -1)
		{
			printf("\n\n Queue is Empty\n");
			break;
		}
		if(i == rear)
		{
			printf("\n\t%d %s",str[i].no,str[i].name);
			break;
		}
		printf("\n\t%d %s",str[i].no,str[i].name);
		if(i==(MAX-1)&& front>rear)
			i=0;
		else
			i++;	
	}
}