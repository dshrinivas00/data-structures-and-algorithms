 #include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct circularLL
{
	int no;
	struct circularLL * nxt;
}cir;

cir* create(cir*);
cir* insertion(cir*,int);
cir* deletion(cir*,int);
void disp(cir*);

void main()
{
	cir * start= NULL;
	int ch,pos;
	while(1)
	{
		printf("\n******* CLL Menu Driven *******");
		printf("\n\t 1.create");
		printf("\n\t 2.Insert");
		printf("\n\t 3.Delete");
		printf("\n\t 4.Print");
		printf("\n\t 5.Exit");
		printf("\n********************************\n");
	
		printf("\n\t Enter your choice : ");
		scanf("%d",&ch);
	
		switch(ch)
		{
			case 1://Create
				start =create(start);
				break;
			case 2://Insert
				printf("\n\t Enter the position to insert the node : ");
				scanf("%d",&pos);
				start = insertion(start,pos);
				break;
			case 3://Delete
				printf("\n\t Enter the position to delete the node : ");
				scanf("%d",&pos);
				start = deletion(start,pos);
				break;
			
			case 4://Print
				disp(start); 
				break;
			case 5://Exit
				exit(0);
			default:
				printf("\n\t Invalid Choice");

		}
	}
}
cir* create(cir*lst)
{
	cir*nw;
	int ch;
	do{
		nw=(cir*)malloc(sizeof(cir));
		printf("\nEnter value :");
		scanf("%d",&nw->no);

		if(lst ==NULL)
			{
				lst=nw;
				lst->nxt=nw;
			}

		else
		{
			nw->nxt=lst->nxt;
			lst->nxt=nw;
		}

		lst=nw;

		printf("\n Do u want more records(1/0): ");
		scanf("%d",&ch);
	}while(ch==1);

 	return lst;
}

cir* insertion(cir* lst, int pos)
{
	cir*p = lst, *nw = NULL;
	int i=0;
	if(lst!=NULL)
	{
		if(pos == 1)
	{
		nw = (cir* )malloc(sizeof(cir));
		printf("\n\t Enter the no");
		scanf("%d",&nw->no);
		nw->nxt = lst->nxt;
		lst->nxt = nw;
		printf("\n\tNode is inserted at %d position",pos);
		return lst;
		
	}
	else
	{
		do
		{
			p=p->nxt;
			i++;
			if(pos-1 == i && p==lst)
			{
				nw = (cir*)malloc(sizeof(cir));
				printf("Enter the no : ");
				scanf("%d",&nw->no);
				nw->nxt = lst->nxt;
				lst ->nxt = nw;
				lst = nw;
				printf("\n\tNode is inserted at %d position",pos);
				return lst;
			}
			else if(pos-1 == i)
			{
				nw = (cir*)malloc(sizeof(cir));
				printf("Enter the no : ");
				scanf("%d",&nw->no);
				nw->nxt = p->nxt;
				p->nxt = nw ;
				printf("\n\tNode is inserted at %d position",pos);
				return lst;
			}	
				
		}while(p!=lst);
	}
	printf("\n\tInvalid Position");
	return lst;
	
	}
	else
	{
		printf("\n\t Circular Linkedlist is Empty...Create First");
		return lst;
	}
}
	

cir* deletion(cir* lst, int pos)
{
	cir*p = lst->nxt,*temp=NULL;
	int i=1;
	
	if(lst!=NULL)
	{
		if(pos == i)
	{
		
		lst->nxt = p->nxt;
		free(p);
		printf("\n\tNode is deleted at %d position",pos);
		return lst;
		
	}
	else
	{
		do
		{
			p=p->nxt;
			i++;
			if(pos == i)
			{
				for(temp=lst ; temp->nxt!=p;temp = temp->nxt);
				temp->nxt=p->nxt;
				if(p==lst)
				{
					lst=temp;
				}
				
				free(p);
				printf("\n\tNode is deleted at %d position",pos);
				return lst;
			}
			
				
		}while(p!=lst);
	}
	printf("Invalid Position");
	return lst;
	}
	else
	{
		printf("Empty Circular LInkedlist");
	return lst;
	}
	
}	

void disp(cir * lst)
{
	 cir*p;
	p=lst;
	printf("\nDisplay values :");
	do{
		printf("\n%d",p->nxt->no );
		p=p->nxt;

	}while(p!=lst);
	
	
}