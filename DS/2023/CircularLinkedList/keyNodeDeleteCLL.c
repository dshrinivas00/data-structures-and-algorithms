 #include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct circularLL
{
	int no;
	struct circularLL * nxt;
}cir;

cir* create(cir*);
void disp(cir*);
cir* deletion(cir*, int);

void main()
{
	cir * lst= NULL;
	int key=0;
	lst = create(lst);
	disp(lst);
	printf("\n\t Enter key to delete node : ");
	scanf("%d",&key);
	lst = deletion(lst, key);
	disp(lst);
}
cir* create(cir*lst)
{
	cir*nw;
	int ch;
	do{
		nw=(cir*)malloc(sizeof(cir));
		printf("\nEnter value :");
		scanf("%d",&nw->no);

		if(lst == NULL)
			{
				lst=nw;
				lst->nxt=nw;
			}

		else
		{
			nw->nxt=lst->nxt;
			lst->nxt=nw;
		}

		lst=nw;

		printf("\n Do u want more records(1/0): ");
		scanf("%d",&ch);
	}while(ch==1);

 	return lst;
}

cir* deletion(cir* lst, int key)
{
	cir*p = lst->nxt,*temp=lst;
	
	if(p->no == key && p==lst->nxt)
	{
		
		lst->nxt = p->nxt;
		free(p);
		printf("\n\t Deleted first node");
		return lst;
		
	}
	else
	{
		do
		{
			p=p->nxt;
			if(p->no == key )
			{
				for(temp=lst ; temp->nxt!=p;temp=temp->nxt);
				temp ->nxt = p->nxt;
				if(p==lst)
				{
					lst = temp;
					printf("\n\t Deleted last node");
				}
					
				free(p);
				
				return lst;
			}
			
				
		}while(p!=lst->nxt);
	}
	printf("Invalid key");
	return lst;
	
}


void disp(cir * lst)
{
	 cir*p;
	p=lst;
	printf("\nDisplay values :");
	do{
		printf("\n%d",p->nxt->no );
		p=p->nxt;

	}while(p!=lst);
	
	
}