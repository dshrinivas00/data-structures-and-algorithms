 #include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct circularLL
{
	int no;
	struct circularLL * next;
	struct circularLL * prev;
}cir;

cir* create(cir*);
void disp(cir*);
cir* deletion(cir*,int);

void main()
{
	cir * lst= NULL;
	int pos=0;
	lst = create(lst);
	printf("\n\t List is created successfully\n");
	disp(lst);
	printf("\n\tEnter the position to insert node : ");
	scanf("%d",&pos);
	lst = deletion(lst,pos);
	disp(lst);
}
cir* create(cir*lst)
{
	cir*nw;
	int ch;
	do{
		nw=(cir*)malloc(sizeof(cir));
		printf("\nEnter value :");
		scanf("%d",&nw->no);

		if(lst ==NULL)
			{
				lst=nw;
				lst->next=nw;
				lst->prev = nw;
			}

		else
		{
			nw->next=lst->next;
			lst->next=nw;
			nw->prev = lst;
			nw->next->prev = nw;
		}

		lst=nw;

		printf("\n Do u want more records(1/0): ");
		scanf("%d",&ch);
	}while(ch==1);

 	return lst;
}

cir* deletion(cir * lst,int pos)
{
	 cir* p = lst->next;
	 int i =1;
	 
		 do{
			 
		 if(pos == i)
		{
		p->prev->next = p->next;
		p->next->prev = p->prev;
				
		if(p==lst)
			lst=p->prev;
		
			free(p);
			return lst;
		
		}
		
			p=p->next;
			i++;
		 
	 }while(p!=lst->next);
	 
	 
	printf("\n\t Invalid Position");
	return lst;
	 
}

void disp(cir * lst)
{
	 cir*p;
	p=lst;
	printf("\nDisplay Entered values :");
	do{
		
		printf("\n%d",p->next->no );
		
		p=p->next;

	}while(p!=lst);
	
	
}