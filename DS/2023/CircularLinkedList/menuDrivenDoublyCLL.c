 #include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct circularLL
{
	int no;
	struct circularLL * next;
	struct circularLL * prev;
}cir;

cir* create(cir*);
cir* insertion(cir*,int);
cir* deletion(cir*,int);
void disp(cir*);

void main()
{
	cir * start= NULL;
	int ch,pos;
	while(1)
	{
		printf("\n******* DoublyCLL Menu Driven *******");
		printf("\n\t 1.create");
		printf("\n\t 2.Insert");
		printf("\n\t 3.Delete");
		printf("\n\t 4.Print");
		printf("\n\t 5.Exit");
		printf("\n***************************************\n");
	
		printf("\n\t Enter your choice : ");
		scanf("%d",&ch);
	
		switch(ch)
		{
			case 1://Create
				start =create(start);
				break;
			case 2://Insert
				printf("\n\t Enter the position to insert the node : ");
				scanf("%d",&pos);
				start = insertion(start,pos);
				break;
			case 3://Delete
				printf("\n\t Enter the position to delete the node : ");
				scanf("%d",&pos);
				start = deletion(start,pos);
				break;
			
			case 4://Print
				disp(start); 
				break;
			case 5://Exit
				exit(0);
			default:
				printf("\n\t Invalid Choice");

		}
	}
}
cir* create(cir*lst)
{
	cir*nw;
	int ch;
	do{
		nw=(cir*)malloc(sizeof(cir));
		printf("\nEnter value :");
		scanf("%d",&nw->no);

		if(lst ==NULL)
			{
				lst=nw;
				lst->next=nw;
				lst->prev = nw;
			}

		else
		{
			nw->next=lst->next;
			lst->next=nw;
			nw->prev = lst;
			nw->next->prev = nw;
		}

		lst=nw;

		printf("\n Do u want more records(1/0): ");
		scanf("%d",&ch);
	}while(ch==1);

 	return lst;
}

cir* insertion(cir* lst, int pos)
{
	cir*p = lst, *nw = NULL;
	int i=0;
	if(lst!=NULL)
	{
		if(pos == 1)
	 {
		 nw = (cir*)malloc(sizeof(cir));
		printf("\n\t Enter the no : ");
		scanf("%d", &nw->no);
		nw->next = p;
		nw->prev = p->prev;	
		p->prev->next = nw;
		p->prev = nw;
		return lst;
	 }
	 else{
		 do{
			p=p->next;
			i++;
		 
		 if(pos == i)
		{
		nw = (cir*)malloc(sizeof(cir));
		printf("\n\t Enter the no : ");
		scanf("%d", &nw->no);
		nw->next = p;
		nw->prev = p->prev;	
		p->prev->next = nw;
		p->prev = nw;
		
		if(nw==lst->next)
			return nw;
		
		return lst;
		}
		 
	 }while(p!=lst->next);
	 }
	printf("\n\tInvalid Position");
	return lst;
	
	}
	else
	{
		printf("\n\t Circular Doubly Linkedlist is Empty...Create First");
		return lst;
	}
}
	

cir* deletion(cir* lst, int pos)
{
	cir*p = lst->next;
	int i=1;
	
	if(lst!=NULL)
	{
		 do{
			 
		 if(pos == i)
		{
		p->prev->next = p->next;
		p->next->prev = p->prev;
				
		if(p==lst)
			lst=p->prev;
		
			free(p);
			return lst;		
		}
		
			p=p->next;
			i++;
		 
	 }while(p!=lst->next);
	 
	 printf("Invalid Position");
	return lst;
	}
	else
	{
		printf("Empty Circular LInkedlist");
	return lst;
	}
	
}	

void disp(cir * lst)
{
	 cir*p;
	p=lst;
	printf("\nDisplay Entered values :");
	do{		
		printf("\n%d",p->next->no );		
		p=p->next;
	}while(p!=lst);	
}