#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<process.h>

typedef struct stack
{
	int no;
	char name[10];
	struct stack * next;
}stk;

stk* push(stk*);
stk* pop(stk*);
void disp(stk*);

int main()
{
	stk*top=NULL;
	int ch; 

    while(1)
    {
	printf("\n********************************");
	printf("\nStack With Linked List");
	printf("\n********************************");
	printf("\n 1.PUSH");
	printf("\n 2.POP");
	printf("\n 3.Display");
	printf("\n 4.Exit");
	printf("\n********************************");
	printf("\nEnter the choice : ");
	scanf("%d",&ch);


	switch(ch)
	{
		case 1 : top = push(top);
		         break;
		case 2 : top = pop(top);
		         break;
		case 3 : disp(top);
		         break;
		default : exit(0);

	}

    }
	return 0;
}

stk* push(stk* top)
{
	stk * nw =NULL;
	nw = (stk*)malloc(sizeof(stk));
	printf("Enter the no and name : ");
	scanf("%d %s", &nw->no,nw->name);
	nw->next = top;
	top = nw;
	printf("\n\t Node is pushed...");
	return top;
}

stk* pop(stk*top)
{
	stk*lst = top;
	if(top!=NULL)
	{
		top = top->next;
		free(lst);
		lst = top;
		printf("\n\t Node is poped...");
		return top;
	}
	else{
		printf("\n\t List is empty...");
		return top;
	}
}

void disp(stk * top)
{
	for(;top!=NULL;top=top->next)
	{
		printf("\n\t%d %s\n",top->no,top->name);
	}
}