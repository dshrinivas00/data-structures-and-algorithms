#include<stdio.h>
#include<conio.h>
#include<malloc.h>
#include <string.h>

typedef struct singleLinkedList
{
	int no;
	char name[20];
	struct singleLinkedList * nxt;
}s;

s* create(s*);
s* insertion(s*,int);
s* deletion(s*,int);
void rev(s*);
void disp(s*); 

int main()
{
	s*start=NULL,*head=NULL;
	int ch,pos;
	
	while(1)
	{
		printf("\n******* SLL Menu Driven *******");
		printf("\n\t 1.create");
		printf("\n\t 2.Insert");
		printf("\n\t 3.Delete");
		printf("\n\t 4.Reverse");
		printf("\n\t 5.Print");
		printf("\n\t 6.Exit");
		printf("\n********************************\n");
	
		printf("\n\t Enter your choice : ");
		scanf("%d",&ch);
	
		switch(ch)
		{
			case 1://Create
				start =create(start);
				break;
			case 2://Insert
				printf("\n\t Enter the position : ");
				scanf("%d",&pos);
				start = insertion(start,pos);
				break;
			case 3://Delete
				printf("\n\t Enter the position : ");
				scanf("%d",&pos);
				start = deletion(start,pos);
				break;
			case 4://Reverse
				rev(start);
				break;
			case 5://Print
				disp(start); 
				break;
			case 6:
				exit(0);
			default:
				printf("\n\t Invalid Choice");

		}
	}
	return 0;
}

s* create(s*head)
{
	s* nw=NULL,*lst=NULL;
	int ans=0;
	
	do
	{
		nw = (s*)malloc(sizeof(s));
		nw->nxt = NULL;
		printf("Enter int value : ");
		scanf("%d",&nw->no);
		printf("Enter name : ");
		scanf("%s",nw->name);
		
		if(head == NULL)
		head = nw;
		
		else 
		{
			lst->nxt = nw;
		}
		lst = nw;
		
		printf("Do you want more nodes? (1/0): ");
		scanf("%d",&ans);
		
	}while(ans==1);
	return head;
}

s* insertion(s*head,int pos)
{
	s*nw=NULL,*p = head;
	int node=1;
	if(head != NULL)
	{
		if(node==pos)
		{
			nw = (s*)malloc(sizeof(s));
			printf("Enter new no and name :");
			scanf("%d%s",&nw->no,nw->name);
			nw->nxt = p;
			head = nw;
			return head;
		}
		for(;p!=NULL; p=p->nxt,node++)
		{
			if(node == pos-1)
			{
				if(p->nxt==NULL)
				{
					nw = (s*)malloc(sizeof(s));
			        printf("Enter new no and name :");
					scanf("%d%s",&nw->no,nw->name);
					nw->nxt = NULL;
					p->nxt=nw;
					return head;
				}
				else
				{
					nw = (s*)malloc(sizeof(s));
					printf("Enter new no and name :");
					scanf("%d%s",&nw->no,nw->name);
					nw->nxt = p->nxt;
					p->nxt=nw;
			
				return head;
				}
			}
		}
		printf("### Invalid Position : List remain same ###");
			return head;
	}
	else
	{
		printf("\n\t Linked List is Empty...");
		return head;
	}
}

s* deletion(s*head,int pos)
{
	s*q=head,*p = head;
	int node=1;
	if(head != NULL)
	{
		if(node==pos)
		{
			head = p->nxt;
			free(p);
			return head;
		}
		for(;p!=NULL; p=p->nxt,node++)
		{
			if(node == pos)
			{
				if(p->nxt==NULL)
				{
					q->nxt=NULL;
					free(p);
					return head;
				}
				else
				{
					
					q->nxt = p->nxt;
					free (p);
			
				return head;
				}
			}
			q = p;
		}
		printf("### Invalid Position : List remain same ###");
			return head;
	}
	else
	{
		printf("\n\t Linked List is Empty...");
		return head;
	}
}

void rev(s*p)
{
	if(p!=NULL)
	{
		rev(p->nxt);
		printf("\n %d %s",p->no,p->name);
	}
	
}

void disp(s*p)
{
	if(p==NULL)
	{
		printf("\n\t Linked List is Empty...");
	}
	else
	{
		for(;p!=NULL ; p=p->nxt)
		{
			printf("\n%d %s",p->no,p->name);
		}
	}
	
}