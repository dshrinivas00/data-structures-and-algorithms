#include<stdio.h>
#include<conio.h>
#include<malloc.h>
#include <string.h>

typedef struct singleLinkedList
{
	int no;
	char name[20];
	struct singleLinkedList * nxt;
}s;

s* create1(s*head);
void swap(s*p);
void disp(s*p); 

int main()
{
	s*start=NULL,*head=NULL;
	start = create1(head);
	disp(start);
	swap(start);
	disp(start);
	
	return 0;
}

s* create1(s*head)
{
	s* nw=NULL,*lst=NULL;
	int ans=0;
	
	do
	{
		nw = (s*)malloc(sizeof(s));
		nw->nxt = NULL;
		printf("Enter int value : ");
		scanf("%d",&nw->no);
		printf("Enter name : ");
		scanf("%s",nw->name);
		
		if(head == NULL)
		head = nw;
		
		else 
		{
			lst->nxt = nw;
		}
		lst = nw;
		
		printf("Do you want more nodes? (1/0): ");
		scanf("%d",&ans);
		
	}while(ans==1);
	return head;
}

void swap(s*p)
{
	s* q = NULL, temp;
	
	for(;p!=NULL ; p=q->nxt)
	{
		q=p->nxt;
		if(q==NULL)
		break;
		
		temp.no = q->no;
		strcpy(temp.name,q->name);
		q->no = p->no;
		strcpy(q->name,p->name);
		p->no = temp.no;
		strcpy(p->name,temp.name);
	}
	
}

void disp(s*p)
{
	for(;p!=NULL ; p=p->nxt)
	{
		printf("\n%d %s",p->no,p->name);
	}

}