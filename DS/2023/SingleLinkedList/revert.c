#include<stdio.h>
#include<conio.h>
#include<malloc.h>
#include <string.h>

typedef struct singleLinkedList
{
	int no;
	char name[20];
	struct singleLinkedList * nxt;
}s;

s* create1(s*);
s* revert(s*);
void disp(s*); 

int main()
{
	s*start=NULL,*head=NULL;
	start = create1(start);
	disp(start);
	head=revert(start);
	
	printf("\n display after revert");
	disp(head);
	
	
	return 0;
}

s* create1(s*head)
{
	s* nw=NULL,*lst=NULL;
	int ans=0;
	
	do
	{
		nw = (s*)malloc(sizeof(s));
		nw->nxt = NULL;
		printf("Enter int value : ");
		scanf("%d",&nw->no);
		printf("Enter name : ");
		scanf("%s",nw->name);
		
		if(head == NULL)
		head = nw;
		
		else 
		{
			lst->nxt = nw;
		}
		lst = nw;
		
		printf("Do you want more nodes? (1/0): ");
		scanf("%d",&ans);
		
	}while(ans==1);
	return head;
}

s* revert(s*head)
{
	s*p=head, *q, *r, *t;
	if(p==NULL || p->nxt ==NULL)
		return head;
	q=p->nxt;
	r=q->nxt;
	while(r!=NULL && r->nxt!=NULL)
	{
		t=r->nxt;
		r->nxt = t->nxt;
		t->nxt = q->nxt;
		q->nxt= t;
	}
	

	
		t= q->nxt;
		r->nxt=q;
	

		q->nxt=p;
		p->nxt=NULL;
		return t;
	
	
}

void disp(s*p)
{
	for(;p!=NULL ; p=p->nxt)
	{
		printf("\n%d %s",p->no,p->name);
	}
}